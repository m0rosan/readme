---
date: 2021-12-01T15:34:15-05:00
title: "Secure Credentials for CICD with Vault"
description: "How to securely manage credentials in CICD systems with Vault"
featured_image: "/images/devsecops.jpeg"
tags: ["kubernetes", "vault", "aws", "cicd", "ephemeral credentials", "security"]
disable_share: true
---


We have discussed how we programatically generate ephemeral credentials via Vault for Artifactory to publish/resolve artifacts, for GitLab to generate Project Access Token. We have created a vault plugin for each use case. We can talk about how we securely manage credentials for other systems such as Kubernetes, AWS, GCP in CICD systems.

Security is at high in priority. However, integrating security mindset in developer workflow is hard, especially as we pass down CICD to developers and at scale.  As we manage a few dozens of kubernetes clusters, 100s of AWS accounts, a few dozens of GCP Projects, millions of artifacts and 10s of 1000s of git projects that runs CI and CD all the time, we can't scale to generate service accounts for each service and rotate every X days. We have tackled to remove this manual toils and provide more autonomous and more secure way to manage all credentials in CICD system.

## How it works

There are 2 parts.

1. [vault resource deploy time]: a user defines what resource and capabilities a pipeline can have. This is defined in yaml format and a user sends Merge Request for review. In each MR, we perform auto-validation on each resource type and capabilities so that all resources follows our security policies. Once it is merged, its pipeline creates Vault resource for the pipeline. Depending on the service type, it'll create a service account or setup RBAC on it.
1. [pipeline runtime]: a user runs a pipeline and request a token for a desired system to Vault. Vault, then, generates a token with predefined Time-to-Live(TTL) or user defined TTL within defined maximum TTL for the requested system and returns the token to the caller pipeline. The pipeline can use the token to perform needful.

**Bonus**: We create CLI for CI that abstracts interaction with vault and sets proper configuration format for each resource such as KUBECONFIG, DOCKER_CONFIG, etc

## Systems

### Kubernetes

We utilize [Kubernetes Auth Backend] in Vault and kubernetes RBAC. In each pipeline, we'll provide ephemeral credential and CA data

In this example, we'll have 2 roles - namespace-admin and namespace-read

During Namespace creation, we perform below to prepare namespaces and RBAC to be consumed in CICD

1. create namespace
1. create RBAC user for namespace-admin and namespace-read
1. create ClusterRoles for namespace-admin and namespace-read. Add adequate rules
1. create RoleBindings for appropriate ClusterRole
    - we also bind individual users, groups and serviceaccounts to a namespace which will be used in local CLI in bonus section

During pipeline, a pipeline can request Vault to generate a token for either namespace-admin or namespace-read for a namespace in a cluster.

### AWS

We use a AWS Vault Plugin which you can find in [their official document][aws secret]. We use Security Token Service(STS) to assume an IAM role in a respective account. This allows us to follow a least priviledge practice when it comes to a resource creation/deletion in each pipeline.

1. Follow the documentation to onboard an AWS account
1. Create a generic CICD role that can provision IAM roles and policies
1. Use the generic CICD role in a centralized pipeline to create other CICD roles and policies for other pipelines to assume. We can ensure that any newly created CICD roles and policies from this pipeline goes through proper review and it doesn't interfere each other.
1. Each pipeline can assume a created CICD IAM role to create/delete their own resources

### GCP

We use a [GCP Secret Plugin]. At vault resource deploy time, a user can define their service account and its role bindings. At pipeline runtime, a user can request a token for the created service account.

Caveat: a service account can only have 10 keys concurrently. We'll need to make sure revoking a key once it's used than let it expire.

### Artifactory

We have developed a vault plugin, open sourced in <https://github.com/splunk/vault-plugin-secrets-artifactory>. This plugin allows to create artifactory group and permission targets, then generate a token scoped to a created group. You can find a detail in another blog post - [Secure Credential Management for Artifactory with Vault]

### GitLab

We have developed a vault plugin, open sourced in <https://github.com/splunk/vault-plugin-secrets-gitlab>. This plugin allows to generate a project access token. You can find a detail in [This blog post]. When GitLab develops/exposes Groups Access Token functionality, we are planning to add it.

We currently don't plan to make a plugin compatible with service accounts' Personal Access Token due to GitLab pricing model where a service account is counted as license user and incurs some cost. In addition, a service account's PAT can't currently scope down to a project and it imposes more risks than Project (or Group) Access Token if not handled correctly. GitLab is working to add a project scope for a user's PAT. It's yet to come.

## Security Concern

We use GitLab as our CICD system. Each job has JSON Web Token (JWT) provided as CI/CD variable named `CI_JOB_JWT`. This JWT can be used to authenticate with Vault using the [JWT Auth] method. This includes namespace, project id, ref, ref protection, environment, etc. We bind vault resources to pipeline level with vault policies to limit access to certain resources. For example, only main branch pipeline from project 1234 has an access to AWS XYZ CICD IAM role in an account 1, Kubernetes ABC namespace in cluster X, publich docker image to package scoped location in production registry.

Generally, we want to limit changes affecting staging/production resources. We advocate strong auth for everything for auditing and compliance reasons.

## Bonus - Local CLI

As explained in artifactory blog, we also create local CLIs to generate ephemeral credentials for kubernetes clusters and AWS accounts, it is integrated with SAML group access and it ensures MFA for local CLI.
For **kubernetes**, we bind individual users and members of a SAML group to a namespace mentioned above. For a non-production cluster, a user gets namespace admin for their assigned namespaces. For a production cluster, a user has only read access. In case of production incident and a user requires a necessary access, we created an escalation CLI which requires an approval from a escalation approval groups. Once approved, a user can gain an escalated priviledge (namespace admin) in a production cluster.
For **AWS**, SAML group is assigned to IAM Roles. Through CLI, a user can retrieve an ephemeral token scoped to a IAM Role.

## Coming soon

We are developing a vault plugin for SonarQube and use an ephemeral token to run an analaysis, then revoke immediately.  
We created our internal toolings and custom vault setup to achieve this functionality in GitLab and Vault. We hope to contribute back to community and have an official support from GitLab and its external secret with Vault.

[Kubernetes Auth Backend]: https://www.vaultproject.io/docs/auth/kubernetes
[aws secret]: https://www.vaultproject.io/docs/secrets/aws
[GCP Secret Plugin]: https://github.com/hashicorp/vault-plugin-secrets-gcp
[Secure Credential Management for Artifactory with Vault]: https://medium.com/splunk-engineering/secure-credential-management-for-artifactory-with-vault-89316c7bf6ca
[JWT Auth]: https://www.vaultproject.io/docs/auth/jwt#jwt-authentication
