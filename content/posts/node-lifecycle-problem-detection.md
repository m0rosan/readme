---
date: 2021-11-12T15:34:15-05:00
title: "Kubernetes Node Lifecycle Part 1 - Problem Detection"
description: "Detect a node problem and report"
featured_image: "/images/k8s.png"
tags: ["kubernetes", "lifecycle", "problem detection"]
disable_share: true
---

## Introduction

As we manage kubernetes clusters, a node should always be ephemeral. That is, we can spin up when needed and tear down when not necessary. I'll discuss how kubernetes node lifecycle should be in series. There are many phases in its lifecycle, including, but not limited to, static and reusable disk creation(AWS AMI), node provisioning, node termination, problem detection, autoscaling, and spot instances.

In this part, I'll discuss about problem detection in a node. A problem in a node affects service availablity and reliability. It is important to detect a problem, report then remediate automatically by either evicting a node or fixing it.

I have installed [node-problem-detector][npd] in a cluster to make various node problems visible to the upstream layers in the cluster.

## Background

From their background statement,

There are tons of node problems that could possibly affect the pods running on the node, such as:

Infrastructure daemon issues: ntp service down;
Hardware issues: Bad CPU, memory or disk;
Kernel issues: Kernel deadlock, corrupted file system;
Container runtime issues: Unresponsive runtime daemon;
...
Currently, these problems are invisible to the upstream layers in the cluster management stack, so Kubernetes will continue scheduling pods to the bad nodes.

## Problem Detection and Reporting

Base node problem detector is a log based monitoring, which counts occurrence of a user provided pattern in given timeframe. Then it can export it as both counter and gauge prometheus metrics type as well as report it as node status.

Since it's log based, you can provide any log path in node file system such as /var/log/journal or /dev/kmsg.

It also bundles with health checker. It's limited to certain services such as kubelet, kubeproxy and dockerd but it can trigger restart of a service if it's in bad condition.

Node Problem Detector has a plugin architecture where you can bring your own tests and run as custom plugin, which then reports metrics and change node status.

For example, you can

- test all systemd services are running healthy
- test overlay network; you wanna make sure that all nodes are routable and accessible

With metrics, you can trigger an alert and send to your means of communication such as Slack or Paging tool.

With node status updated, we could trigger pod evicition or no new scheduling.

## Synthetic Monitoring

In addition to node problem detection which uses hostNetwork, we can extend custom plugin functionality to synthetic monitoring in a pod with namespace network. Even a node doesn't have a problem, it is possible that your own kubernetes setup hasn't been fully completed or gritching due to some errors.

For example, if you setup

- kiam, a pod should be able to assume it and acquire credentials
- both internal and external facing proxy services, a pod should be able to communicate to it

and many more tests.

This ensures that not only a node problem is reported but pods on a node are functioning as expected.

## Remediation

Node Problem Detector comes with healthchecker for kubelet, kubeproxy, docker and CRI. It can trigger restart of a service when it goes into unhealthy state.

For your own custom tests, you can embed similar functionalities. You should certainly have config management tool such as Ansible or Puppet to keep the node in consistent state but there could be some gap(window).

In next part, I'll talk about node eviction.

[npd]: https://github.com/kubernetes/node-problem-detector
