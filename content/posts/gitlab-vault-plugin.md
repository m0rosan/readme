---
date: 2021-07-19T15:34:15-05:00
title: "Vault Plugin For Gitlab Token"
description: "How to securely manage Gitlab project access token with Vault"
featured_image: "/images/gitlab-vault.png"
tags: ["gitlab", "vault"]
disable_share: true
---

What's the best way to securely manage GitLab token? If you ever wonder, you are in the right place. Gitlab has implemented different kinds of tokens. You can find overview in [their official doc][token overview]

After accessing all tokens, **project access token** is the clear winner for many operations in any environments such as CI/CD pipeline or automation. However, it's missing one security aspect - ephemeral. If you are in a secutiy minded organization, it imposes some threat by having a long lived token in a system. Operationally, it is huge burden to rotate a token every X days, when it's exposed or when a team member leaves.

There is a solution for these credential weaknesses by programmatically generating project access tokensn from secure credential system, Vault. I have developed a [Vault plugin for Gitlab Project Access Token] to generate project access tokens

You can install the plugin into your vault system and get an ephemeral credential in 5 minutes.

1. Install the plugin from [release page] or compile from [the source][Vault plugin for Gitlab Project Access Token], then keep the binary in plugins directory that vault loads
1. Enable secrets backend.

    ```sh
    $ vault enable secrets -path=gitlab vault-plugin-secrets-gitlab
    Success! Enabled the vault-plugin-secrets-gitlab secrets engine at: gitlab/
    ```

1. Configure plugin backend by supplying Gitlab URL and Token

    ```sh
    $ vault write gitlab/config base_url="https://gitlab.example.com" token=$GITLAB_TOKEN
    Key       Value
    ---       -----
    base_url  https://gitlab.example.com
    max_ttl   0s
    ```

    *Tip*: You should define `max_ttl`. Any request for project access token can't exceed this configured max TTL.
1. Generate a project access token

    ```sh
    $ vault write gitlab/token id=1 name=ci-token scopes=api,write_repository expires_at=2021-01-02T00.00.00Z
    Key           Value
    ---           -----
    expires_at    2021-01-02T00:00:00Z
    id            12345
    name          ci-token
    scopes        [api write_repository]
    token         REDACTED_TOKEN
    ```

1. Role-based token creation

    ```sh
    # Create a role
    $ vault write gitlab/roles/project1-ci id=1 name=ci-token scopes=api
    Key          Value
    ---          -----
    id           1
    name         ci-token
    role_name    project1-ci
    scopes       [api]
    token_ttl    24h

    # Generate a token for the role
    $ vault write gitlab/token/project1-ci
    Key           Value
    ---           -----
    expires_at    2021-01-02T00:00:00Z
    id            1234567
    name          ci-token
    scopes        [api]
    token         REDACTED_TOKEN
    ```

## Conclusion

Simply put, managing project access tokens with the Vault plugin is more **secure** and more **autonomous**.  
It is more **secure** by shifting from static credentials to ephemeral credentials. Valid persistent credentials no longer stay in the system, which reduces the risk of leakage/exposure. Even if it's leaked, it's short-lived. With long-lived persistent credentials, porting or leakage of credentials breaks integrity. Short-lived credentials are not portable and minimizes the risk of such behavior.  
It is more **automated** by moving human operation to machine operation when it comes to creating tokens.  

Enjoy and benefit!

[token overview]: https://docs.gitlab.com/ee/security/token_overview.html
[Vault plugin for Gitlab Project Access Token]: https://gitlab.com/m0rosan/vault-plugin-secrets-gitlab
[release page]: https://github.com/splunk/vault-plugin-secrets-gitlab/releases
