---
title: "About"
description: "Software Engineer in Cloud Infrastructure"
featured_image: ''
---
<!-- {{< figure src="/images/Victor_Hugo-Hunchback.jpg" title="Illustration from Victor Hugo et son temps (1881)" >}} -->

## Skills

**Language**: Golang, Python, Bash  
**Automation**: Terraform, Ansible, Puppet  
**Tool**: Docker, Kubernetes, GitLab, Vault, Artifactory  
**Certification**: AWS Solution Architect - Associate, Puppet Certified Professional  

## Experiences

**Software Engineer in Cloud Infrastructure** @Splunk  
Jan 2020 - Current

- Implemented suite of ephemeral credential for CICD pipeline in GitLab with Vault
- Redesigned package management with secure and compliant manner
- Lead development of Vault plugin for Artifactory to resolve and push artifacts with artifact scoped ephemeral token - [open-sourced][vp-artifactory] and incoming blog
- Implemented CD pipeline to reduce deployment time by 95% using Gitlab CICD and Terraform
- Saved $150,000 per month for development server cost by introducing spot instances while maintaining the availability
- Saved $50,000 per month by migrating off SaaS Artifactory to self-hosted while maintaining availability with multi region setup
- Rearchitected self-hosted GitLab SCM server and CICD runners to achieve high availability with zero-downtime upgrade
- Administered company-wide GitLab Server with 2500 users and GitLab Runners with average 4000 CICD builds per hour
- Observed/monitored GitLab Logs/Metrics with Splunk Suite – Splunk, Infra Monitoring(SignalFx), Splunk On-Call(VictorOps) and Prometheus
- Develop k8s microservices with Golang to automate around GitLab
- Participate in on-call rotation for Splunk Cloud services
- Develop and maintain Terraform modules/CI automation that powers Splunk Cloud

## OSS

- [vault-plugin-secrets-artifactory][vp-artifactory]
- [vault-plugin-secrets-gitlab][vp-gitlab]
[vp-artifactory]: https://github.com/splunk/vault-plugin-secrets-artifactory
[vp-gitlab]: https://github.com/splunk/vault-plugin-secrets-gitlab